\documentclass[11pt]{article}
\usepackage[margin=2cm]{geometry}
\usepackage{fancyhdr}
\usepackage{amsmath, amsthm, amssymb}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{float}
\usepackage{changepage}
\usepackage{listings}
\usepackage{multicol}
\usepackage{csvsimple}
\usepackage{tabularx}
\usepackage{color}
\usepackage{dblfnote}
\usepackage{pdfpages}
\graphicspath{ {images/} }
\usepackage[title, titletoc]{appendix}
\usepackage{subfig}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{afterpage}
\usepackage{cite}
\usepackage{booktabs}
\usepackage{subfiles}
\usepackage{siunitx}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage[activate={true,nocompatibility},
final,
tracking=true,
spacing=true,
factor=1100,
stretch=10,
shrink=10]{microtype}


\allowdisplaybreaks

\pagestyle{fancy}
\fancyhf{}
\fancyfoot[C]{\thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0.5pt}
\newcommand{\RN}[1]{%
  \textup{\uppercase\expandafter{\romannumeral#1}}%
}
\makeatletter
\def\blfootnote{\xdef\@thefnmark{}\@footnotetext}
\makeatother
\setlength{\parindent}{0cm}

\definecolor{codegreen}{RGB}{0,128,0}
\definecolor{codegray}{RGB}{193,193,193}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
\definecolor{codeblue}{RGB}{0,0,255}
\definecolor{codered}{RGB}{255,0,0}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},
    commentstyle=\color{codegreen},
    keywordstyle=\color{codeblue},
    deletekeywords={range}
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codered},
    emph={as, range, uint32_t},
    emphstyle=\color{codeblue},
    basicstyle=\linespread{1.1}\footnotesize\ttfamily,
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    numbers=left,
    numbersep=5pt,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2,
    xleftmargin=\parindent,
    language=Python,
    firstnumber=last
}

\lstset{style=mystyle,
}

%\date{}
\title{A Determmination of Expansion Rates Within the Planetary Nebula NGC$6720$ (Ring Nebula) by Optical Means and Narrowband Examination of Structure in the Ionisation Front \& Halos.\bigskip\\\large PHYC40890 --- $4^{th}$ Year Astronomy Field Trip\\Data Analysis Report.}
\author{Damien Whelan --- 14739979}

\begin{document}
%\includepdf{titlepage/titlepage.pdf}
%\newpage
%\mbox{}
%\blfootnote{Cover image\cite{}}\\
%\thispagestyle{empty}
%\newpage
%\thispagestyle{fancy}
%\tableofcontents
%\newpage
%\listoffigures
%\lstlistoflistings
%\mbox{}
%\newpage
\maketitle
%\thispagestyle{fancy}
%\begin{multicols*}{2}
\section{Data Acquisition}\label{sec:acq}
\subsection{Target}
\begin{wrapfigure}{r}{0.5\textwidth}
	\centering
	\includegraphics[width=0.48\textwidth]{M57_CAHA_RECTA_DSA.jpg}
	\caption{Image of the NGC$6720$ (Ring Nebula) from the documentary photo gallery of Calar Alto Observatory. Taken with the \SI{1.23}{\metre} telescope at that facility.\cite{peris2009}}
	\label{fig:calor1}
\end{wrapfigure}
NGC$6720$, the Ring Nebula, is a planetary nebula (PN) located within the constellation Lyra in the northern hemisphere. Like all PN it is the remnant of a sun like star which has reached the end of its red giant stage. The star at this point has ejected its outer layers which are now visible as the PN itself, with the stellar remnant now a white dwarf located at the centre.\\
The nebula is visible at a right ascension (RA) of $18^h53^m35.079^s$ and a declination (DEC) of \ang{+33;01;45.03}. The nebula disk has an angular size of $1.5 \times 1$ arcminutes and a visual magnitude of $8.8$ while the central star has a magnitude of $14.8$\cite{magnitude}.\\
The Ring nebula has several interesting features which will be visible with the available \SI{1.23}{\metre} telescope. It is arranged in a series of concentric shells\cite{guerrero1997}, the innermost being mostly circular, the next elliptical, followed by the petal like inner halo and finally the circular outer halo. The inner ring is a low density core of helium \cite{nasa} where material has been pushed further out by the high speed stellar wind resulting from the increase in temperature of the central star (CS) shortly before it cooled to a white dwarf\cite{odell2007}. Around this is found a molecule rich outer ring (elliptical) and between the two lies an easily visible ionisation front\cite{odell2007}. These components represent the standard view of the ring nebula. The inner and outer halos which surround these are not generally thought of in the popular view of the nebula, however they form an important and interesting part of the structure of the nebula. They are considered to be a remnant of the red giant wind with the petal-like structure of the inner halo formed by a combination of anisotropic excitation as well as interaction with knots, bubbles and outflows within the main shell\cite{guerrero1997}.\\
\subsection{Observation}
In observing NGC$6720$ I will be using several of the telescopes available filter in order to provide the most comprehensive acquisition of data possible. R, B \& V Johnson filters will be required to perform the measurement of the expansion rates. These will provide the most broad view of the nebula allowing for the easiest comparison with previous work. It has been noted that in the previous work carried out by Frew et al.\cite{frew2010}, I and R band Johnson filters were used and it would seem from reading the available literature that should issues such as weather, time or availability of filters play a role in limiting observations that the R band would suffice as this would contain the bright H$_{\alpha}$, N\RN{2} \& S\RN{3} lines.\\
Figure~\ref{fig:calor1} above shows an image composed from data obtained at Calor Alto observatory by Peris et al. over several observations during $2009$. The main features outlined in the target introduction above are clearly visible in the image (inner \& outer rings and inner \& outer halos). Figure~\ref{fig:calor2} shows a decomposition of the image in fig.~\ref{fig:calor1} with the appropriate labels for the narrow and broad band filters used shown on each image. A further discussion of the processing techniques used to generate these images will be undertaken in \S~\ref{sec:proc} below.\\
For exposure times Peris et al. quote \SI{20}{\minute} as the time used for their sub exposures which were combined together to form the composite image shown in fig.~\ref{fig:calor1} above.
\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{m57_multiband.jpg}
	\caption{Image of the ring nebula as seen through the six filters used by Peris et al.\cite{peris2009}}
	\label{fig:calor2}
\end{figure}
Observations will most likely need to be made late in the night at some point from approximately $4$am to sunrise depending on the needs of other observations. Towards the night of the $18^{th}$ the moon will be setting later (towards dawn) as opposed to earlier in the week where it is setting around $3$am, however the target will be higher in the sky on the $18^{th}$. An example visibility plot is shown in fig.~\ref{fig:vis}. In it the moons position can clearly be seen. It should be noted that despite the late setting time it will still be $\sim$\SI{112}{\degree} away from the target and so should not interfere with observations in any meaningful way.\\
\section{Data Processing}\label{sec:proc}
\subsection{Image Reduction}
To begin the analysis of the collected data a reduction will first be performed.\\
\begin{lstlisting}[language=Python, caption={Reduction of target images. This code is based on sample code provided by my project supervisor.\cite{antonio}},label={lst:obj_reduction}, escapechar=\%]
path_3 = '../18mar14/m61/'

target = filelist(path_3)
r_target, Ha_target = filter_checker(target)

trimmed_r_target = trim(r_target)
r_data = ccdproc.CCDData(trimmed_r_target, unit=u.adu)

r_bias_subtracted = ccdproc.subtract_bias(r_data, master_bias)
r_reduced_image = ccdproc.flat_correct(r_bias_subtracted, master_r_flat)
    
r_mean, r_median, r_std = sigma_clipped_stats(r_reduced_image, sigma=10.0, iters=5)
\end{lstlisting}\\

\begin{wrapfigure}{r}{0.5\textwidth}
	\centering
	\includegraphics[width=0.48\textwidth]{18th.png}
	\caption{Example plot of object visibility for the $18^{th}$ March $2019$.}
	\label{fig:vis}
\end{wrapfigure}
Listing~\ref{lst:obj_reduction} shows the steps which are taken in the reduction of an object image observed with the R band filter. Firstly the \lstinline[language=Python]$path$ to the image directory is set, next a list of the available images is generated using the \lstinline[language=Python]$filelist$ function. The header for each image is then checked and the images separated by filter type using the \lstinline[language=Python]$filter_checker$ function. Each image in the file list trimmed to the correct shape using the \lstinline[language=Python]$trim$ function. This resulting image matrix is then converted to ADU by the \lstinline[language=Python]$ccdproc.CCDData$ function. These steps are the same or similar to those carried out for the bias and flat field images. For the object images the master bias and master flat images are then used in the final reduction seen in lines $9$ \& $10$. The last step performed generates our final stacked image, \lstinline[language=Python]$r_mean$ in this case. An identical process is carried out for other filters used. The reduced images are then saved as a fits file for further analysis.
\subsection{Image Processing}
For this step the method employed by Peris et al. will be heavily relied upon, namely; the Richardson-Lucy (RL) deconvolution algorithm\cite{richardson1972} and the wavelet decomposition technique\cite{mertens2014}.\\
\subsubsection{Deconvolution}
\alglanguage{pseudocode}
\begin{algorithm}
\caption{Richardson-Lucy deconvolution algorithm.}
\label{alg:rich-lucy}
\begin{algorithmic}[1]
\State \textbf{Degraded Image} H
\State \textbf{Kernel} $S$
\State \textbf{Initial Estimate} $W_0 = H * S$
\State \textbf{Input Number of Iteration} $n$
\newline
\For{iteration $1$ to $n$}
\State subtract from blurred image $b = H - W_i * S$
\State add the error corrected image $a = b + W_i$
\State set image for next iteration $W_i = a$
\EndFor
\end{algorithmic}
\end{algorithm}\\
\newpage
Deconvolution is, simply put, an algorithmic process for reversing the effects of convolution on recorded data. In his discussion of the topic Richardson describes it in the following manner. Consider a degraded image $H$ which is of the form $H=W\otimes S$, where $W$ is the original image, $S$ is the point spread function (PSF) (often referred to as the kernel) and $\otimes$ is the convolution operator. This algorithmic method then serves to deconvolve $H$, separating out $S$ and thus restoring the desired image $W$.\\
Algorithm~\ref{alg:rich-lucy} is a simplified pseudo code application of the RL algorithm which is shown more completely in eq.~\ref{eq:rich-lucy}. The algorithm itself can be used for so-called blind deconvolution where the kernel is unknown, however, an estimation of the kernel can be made by closely examining reference stars present in the image. The image of the reference star may be Gaussian or Lorentzian in profile, or anything between these extremes, and from this profile the PSF can be estimated. From there it is simply a matter of determining the number of iterations needed to produce the desired result. This can be done by inspection.\\
\begin{equation}
\hat {u}}_{j}^{(t+1)}={\hat {u}}_{j}^{(t)}\sum _{i}{\frac {d_{i}}{c_{i}}}p_{ij}\\
\label{eq:rich-lucy}
\end{equation}
where
\begin{equation}
c_{i}=\sum _{j}p_{ij}{\hat {u}}_{j}^{(t)}
\end{equation}\\
\subsubsection{Decomposition}
Once the image has been properly reduced and deconvolved feature recognition can be achieved by means of the wavelet transforms. According to Mertens et al. wavelets provide a method to decompose the overall structure in an image into a robust set of statistically significant structural patterns\cite{mertens2014}. While this process will not be necessary for the determination of expansion rates within the PN, it will no doubt prove invaluable in the examination of ionisation front and halo structures. In their development of the WISE method for multiscale decomposition, segmentation \& tracking, Mertens et al. utilised the \'a trou wavelet transform shown in eq.~\ref{eq:atrou}.\\
\begin{equation}
f(x,y) = \sum_{j=1}^{J}w_j(x,y)+c_J(w,y)
\label{eq:atrou}
\end{equation}\\
Here $w_j$ is a set of what are known as wavelet scales or resolution-related views of the image. This process will be implemented using the python module \lstinline[language=Python]$PyWavelets$\cite{pywavelets}. The stationary wavelet transform, \'a trou algorithm, is a translation-invariance modification of the Discrete Wavelet Transform that does not decimate coefficients at every transformation level. This function is provided by the \lstinline[language=Python]$PyWavelets$ module and its stationary wavelet transform or \lstinline[language=Python]$swt2$ function. An example of its use is show in listing~\ref{lst:pywt}.\\
\begin{lstlisting}[language=Python, caption={Example usege of PyWavelets stationary wavelet transform function.},label={lst:pywt}, escapechar=\%]
pywt.swt2(image, wavelet, levels, start_level)
\end{lstlisting}\\
This code returns a set of coefficients arrays (shown below) over the range of decomposition levels as follows where cA is approximation, cH is horizontal details, cV is vertical details, cD is diagonal details and m is \lstinline[language=Python]$start_level$. An example of in image which has been processed in this manner is shown in fig.~\ref{fig:pywt}.\\
\begin{algorithm}
\caption*{General results of \lstinline[language=Python]$pywt.swt2$}
\begin{algorithmic}[1]
\State [
\State \hskip1.0em (cA$_$n, (cH$_$n, cV$_$n, cD$_$n)),
\State \hskip1.0em (cA$_$n$+1$, (cH$_$n$+1$, cV$_$n$+1$, cD$_$n$+1$)),
\State \hskip1.0em ...,
\State \hskip1.0em (cA$_$n$+$level, (cH$_$n$+$level, cV$_$n$+$level, cD$_$n$+$level))
\State ] 
\end{algorithmic}
\end{algorithm}\\
\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\linewidth]{plot_mallat_2d.png}
	\caption{Image showing the result of application of \lstinline[language=Python]$PyWavelets$.}
	\label{fig:pywt}
\end{figure}\\
\subsection{Expansion Rate}
To determine the expansion rate of the nebula archival data will be required. This may be available directly from the Calor Alto database in which case calculation may be simplified. As was discussed above, data from Calor Alto was used by Peris et al.\cite{peris2009} in their generation of the images shown in fig.s~\ref{fig:calor1}\&\ref{fig:calor2} above. Direct comparison of the reduced and deconvolved data with the archival data will be made through use of the normalisation technique outlined by O'Dell et al.\cite{odell2009}. This involves aligning the images by means of reference stars, ensuring they are of the same size (pixel by pixel) and normalising them so that the total signal is the same then taking the ratio of the two. From there the expansion rate can be calculated using the time between epochs and the angular resolution of the images. Constraints will be placed on these results by means of the simpler but less accurate magnification technique\cite{odell2009}\cite{hartigan2001}. This technique involves again aligning the images but this time magnifying the older image until features in the nebula itself are aligned and not the background reference stars. The level of magnification is then proportional to the expansion of the nebula. Both of these processes will be implemented for each filter used in observation.\\
The processes outlined here will be carried out using the ds$9$ software package and will involve only data taken using the broadband Johnson filters. A weighting will be applied to the available filters based on the level of structure present in each as outlined by O'Dell et al.\cite{odell2009}\\
\subsection{Individual Features}
The examination of individual features will be performed using the wavelet decomposition algorithm outlined above. This will also be implemented for individual filters used, however only narrowband filtered images will be required here. By examining the highlighted wavelet scales, several features of the PN will be highlighted. In the main ring it is expected that knots will be observed. As is pointed out in several sources, knots within the PN will appear mainly in extinction, however those close to the ionisation front will provide some emission in the N\RN{2} band and should be most visible in the O\RN{3} band. After identification of the features a slice will be taken through the image at the relevant position angle. A profile of the feature will then be made in each of the filters and from these a determination of its interaction with the other material of the PN, the possible driving/limiting mechanisms and local kinematics of the object will be made.\\
The inner and outer halos are most visible in the H$2$ band, as can be clearly seen in fig.~\ref{fig:calor2} above. In \S\ref{sec:acq} I briefly discussed the mechanisms responsible for the structure of these halos proposed by Guerrero et al. Their claim that the structure is a result of interaction between the red giant wind, which makes up the halos, and the main ring can be investigated by closely examining the boundary between them. Several sources, including Guerrero et al.\cite{guerrero1997} and O'Dell et al.\cite{odell2009}, indicate that the outermost part of the outer ring is brightest in the O\RN{3} filter. The image in fig.~\ref{fig:calor2} would seem to substantiate these claims. An examination of images from these two filters at key areas, notably north and north-east on the O\RN{3} image in fig.~\ref{fig:calor2} where there appears to be some protrusion of the outer ring into the inner halo, should provide some insight into the underlying mechanism for the structure responsible for the inner halo.\\
\section{Project Structure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\linewidth]{flow.png}
	\caption{Chart showing the outline of operations for the project.}
	\label{fig:flow}
\end{figure}\\
\newpage
\vfill
\vfill

{\color{codegray}\hline}
\vspace{5pt}

\footnotesize{
\bibliographystyle{acm}
\bibliography{analysis.bib}
}

\end{document}
