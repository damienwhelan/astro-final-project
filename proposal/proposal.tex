\documentclass[11pt]{article}
\usepackage[margin=2cm]{geometry}
\usepackage{fancyhdr}
\usepackage{amsmath, amsthm, amssymb}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{float}
\usepackage{changepage}
\usepackage{listings}
\usepackage{multicol}
\usepackage{csvsimple}
\usepackage{tabularx}
\usepackage{color}
\usepackage{dblfnote}
\usepackage{pdfpages}
\graphicspath{ {images/} }
\usepackage[title, titletoc]{appendix}
\usepackage{subfig}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{afterpage}
\usepackage{cite}
\usepackage{booktabs}
\usepackage{subfiles}
\usepackage{siunitx}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage[activate={true,nocompatibility},
final,
tracking=true,
spacing=true,
factor=1100,
stretch=10,
shrink=10]{microtype}


\allowdisplaybreaks

\pagestyle{fancy}
\fancyhf{}
\fancyfoot[C]{\thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0.5pt}
\newcommand{\RN}[1]{%
  \textup{\uppercase\expandafter{\romannumeral#1}}%
}
\makeatletter
\def\blfootnote{\xdef\@thefnmark{}\@footnotetext}
\makeatother
\setlength{\parindent}{0cm}

\definecolor{codegreen}{RGB}{0,128,0}
\definecolor{codegray}{RGB}{193,193,193}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
\definecolor{codeblue}{RGB}{0,0,255}
\definecolor{codered}{RGB}{255,0,0}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},
    commentstyle=\color{codegreen},
    keywordstyle=\color{codeblue},
    deletekeywords={range}
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codered},
    emph={as, range, uint32_t},
    emphstyle=\color{codeblue},
    basicstyle=\linespread{1.1}\footnotesize\ttfamily,
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    numbers=left,
    numbersep=5pt,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2,
    xleftmargin=\parindent,
    language=Python,
    firstnumber=last
}

\lstset{style=mystyle,
}

\date{}
\title{\Large\textbf{Calor Alto \SI[detect-weight]{1.23}{\metre} Telescope; Observation of NGC\bm{6720}.}}
\author{PI: Damien Whelan}

\begin{document}
\maketitle
\thispagestyle{fancy}
\section*{\large Abstract}
Studies of NGC$6720$ using broad and narrow bandwidth filters have previously shown an expansion rate for the nebula of \SI{0.65}{\kilo\metre\per\second}.\\
This work will seek to follow on from prior studies in these areas, to further constrain values for the rate of expansion of NGC$6720$ as well as measuring development in the evolution of features within the nebula.\\
This observing campaign will require the use of the Calor Alto \SI{1.23}{\metre} telescope using the V and B Johnson broadband filters as well as H$_{\alpha}$ narrowband filter. Exposures of the following durations will be made; \SI{30}{\minute} total for broadband filters and \SI{60}{\minute} for narrowband in \SI{5}{\minute} sub exposures.\\
\section*{\large Scientific Rationale}\label{sec:rat}
Classical planetary nebulae (PNe) are glowing shells of ionised gas ejected from low to intermediate mass stars ($\sim1-8~M_{\odot}$)\cite{frew2010} towards the end of their lives. As it reaches the end of the Asymptotic Giant Branch the star begins to shed its outer layers producing a highly extended atmosphere through a low velocity stellar wind. After this ejection the Central Star (CS) increases in temperature producing a high velocity stellar wind which ionises the ejecta and producing an ionisation front\cite{odell2007} which separates an inner shell from the outer shell. The CS then cools to become a white dwarf seeing out the end of its life in this state. The nebula itself will last for $\sim10^5$ years with the CS surviving for several billion years\cite{fontaine2001} before crystallising into a black dwarf. Around $3000$ PNe are known to exist in the Milky Way (Frew et al. $2010$ and references therein\cite{frew2010}) but despite this no agreed upon definition exists. In recent years, however, some effort has been made to remedy this situation\cite{frew2010}.\\
PNe represent an interesting field of study for several reasons. Once it has reached the end of its hydrogen burning phase, the sun will begin to transition into a red giant and will itself become a PNe. As such, examination of PNe represents a significant method for studying the evolution of intermediate-mass stars. The PNe luminosity function also represents a powerful tool for calculating extragalactic distances (Ciardullo $2010$\cite{ciardullo2010}) while the CS, once it becomes a white dwarf, can be used to constrain the ages of various populations of evolved stars within the Milky Way (Fontaine et al.\cite{fontaine2001}).\\
PNe are generally modelled as being highly stratified with inner and outer shells separated by an ionisation zone generated by the stellar wind from the hot CS. The inner shell is of a lower density than the outer one with the ionisation zone being the most optically bright region of the PNe. According to O'Dell et al. $2009$ \cite{odell2009}, NGC$6720$ follows this model and is highly structured in its construction. It is arranged in a series of concentric shells\cite{guerrero1997}, the innermost being mostly circular, the next elliptical, followed by the petal like inner halo and finally the circular outer halo. The inner ring is a low density core of helium \cite{nasa} where material has been pushed further out by the high speed stellar wind from the CS\cite{odell2007}. Around this is found a molecule rich outer ring (elliptical) and between the two lies an easily visible ionisation front\cite{odell2007}. These components represent the standard view of the ring nebula. The inner and outer halos which surround these are not generally thought of in the popular view of the nebula, however they form an important and interesting part of its structure. They are considered to be a remnant of the red giant wind with the petal-like structure of the inner halo formed by a combination of anisotropic excitation as well as interaction with knots, bubbles and outflows within the main shell\cite{guerrero1997}. This can be clearly seen in the images produced by Peris et al. $2009$\cite{peris2009} which are shown in fig.~\ref{fig:calor2}.\\
Two separate models for the expansion of PNe have been proposed; homologous expansion (O'Dell et al. $2009$\cite{odell2009}), where the velocity field is such that the shape of the object does not change with time except for a scaling factor which is proportional to time and ballistic expansion (Steffen et al. $2010$\cite{inproceedings}) where $dv/dt=0$.\\ 
\begin{figure}[ht]
	\centering
	\includegraphics[width=\linewidth]{m57_multiband.jpg}
	\caption{Image of the NGC$6720$ as seen through the six narrowband filters used by Peris et al.\cite{peris2009}}
	\label{fig:calor2}
\end{figure}\\
\section*{\large Objective of This Observation}\label{sec:obj}
This work will seek to observe the Ring Nebula NGC$6720$. NGC$6720$ is one of the brightest and most extensively studied PNe in the Milky Way. The main objective of these observations will be to determine the expansion rate of the nebula through examination of the overall shape as well as individual features present within the nebula.\\
Data from observations where the broadband filters have been employed will be used in to examine the structure of the inner rings. 
Data from observations where the narrowband filters have been employed will be used in examinations of major features within the nebula. In the main ring it is expected that knots may be observed which, as is pointed out in several sources, will appear mainly in extinction. Filaments within the main ring should also be readily visible in the narrow band. After identification of the features slices will be taken through the image at the relevant position angles and from these a determination of the interaction between the feature and the other material of the PNe and local kinematics of the object will be made.\\
The structure of the inner and outer halos is a result of interaction between the red giant wind, which makes up the halos, and the main ring. This can be investigated by examining the boundary between them in particular key areas, notably north and north-east of the image in fig.~\ref{fig:calor2} where there appears to be some protrusion of the outer ring into the inner halo.\\
Final determination of the nebula expansion will require the use of archival data, which is widely available due to the extensive nature of previous studies of this target. Direct comparison of the observational data with the archival data will be made through use of the normalisation technique outlined by O'Dell et al.\cite{odell2009}. This involves aligning the images by means of reference stars, ensuring they are of the same size (pixel by pixel) and normalising them so that the total signal is the same then taking the ratio of the two. From there the expansion rate can be calculated using the time between epochs and the angular resolution of the images. Constraints will be placed on these results by means of the simpler but less accurate magnification technique\cite{odell2009}\cite{hartigan2001}. This technique involves again aligning the images but this time magnifying the older image until features in the nebula itself are aligned and not the background reference stars. The level of magnification is then proportional to the expansion of the nebula. Both of these processes will be implemented for each filter used in observation.\\
\section*{\large Feasibility of Study}\label{sec:feas}
For this work exposures will be made at a total integration time of \SI{30}{\minute} for the broadband filters and \SI{60}{\minute} for the narrowband. These integration times will be separated into a number of \SI{5}{\minute} sub exposures ($\tau$) which will be stacked during processing. This separation of the exposures will allow for any removal of cosmic ray events which may occur and will also allow for greater flexibility within the observing timetable. These integration times will produce data with a sufficiently high signal to noise ratio (SNR) based on a comparison made with prior work carried out at the Teide Observatory IAC$80$ telescope. IAC$80$ has an aperture of \SI{0.82}{\metre} while for this work the telescope being used will have an aperture of \SI{1.23}{\metre}. Taking the ratio of the square of each of these shows that the captured flux of the \SI{1.23}{\metre} telescope will be $2.25$ times that of the IAC$80$. The flux at a particular point on the target will include the flux of the background, this will be the total flux. SNR is given by the following equation, $SNR=flux_{object}/\sqrt{flux_{total}}$. As has already been shown, the ratio of fluxes from this work to the previous is $2.25$ and so the SNR will have a scaling factor of $1.58$ to correct for the difference in telescope size.\\
Examining the prior work more closely an average value for the flux at several points around the main ring of the target (the bright spots at the north and south of the target and the dim patch at the north east) was taken and corrected for the \SI{1.23}{\metre} telescope. The results of this are shown in table~\ref{tab:comp}.\\
\begin{table}[ht]
	\centering
	\begin{tabular}{lccccccccc}
		\toprule
		Telescope & Filter & $\tau$ (s) & BG & N & SNR & NE & SNR & S &  SNR\\
		\midrule
		\midrule
		IAC$80$ & B & 90 & 530 & 820 & 10  & 590 & 2.5 & 750 & 8\\
		& V & 60 & 540 & 1650 & 27 & 700 & 6 & 1400 & 23\\
		& R & 60 & 590 & 2100 & 33 & 850 & 9 & 1600 & 25\\
		&&&&&&&&&\\
		\SI{1.23}{\metre} & B & 90 & 1192 & 1845 & 15 & 1327 & 4 & 1687 & 12\\
		& V & 60 & 1215 & 3712 & 41 & 1575 & 9 & 3150 & 34\\
		& R & 60 & 1327 & 4725 & 49 & 1912 & 13 & 3600 & 38\\
		\bottomrule
	\end{tabular}
	\caption{Table showing conversion of fluxes from IAC$80$ telescope to \SI{1.23}{\metre} telescope.}
	\label{tab:comp}
\end{table}\\
\begin{wraptable}{r}{7.75cm}
%	\centering
	\begin{tabular}{lccc}
		\toprule
		Filter & Feature & SNR (Old) & SNR (New)\\
		\midrule
		\midrule
		B & N & 15 & 67\\
		& NE & 4 & 17\\
		& S & 12 & 54\\
		&&&\\
		V & N & 41 & 225\\
		& NE & 9 & 49\\
		& S & 34 & 186\\
		&&&\\
		R & N & 49 & 268\\
		& NE & 13 & 71\\
		& S & 38 & 208\\
		\bottomrule
	\end{tabular}
	\caption{Table of updated SNR values for total exposure time of \SI{30}{\minute}.}
	\label{tab:fix}
\end{wraptable}\\
The poor quality of SNR values in table~\ref{tab:comp} means exposure times which were used the prior study will be increased for this work. The recalculated SNR values are presented in table~\ref{tab:fix}. These were calculated in the following way; $SNR_{n\tau}=\sqrt{n}~SNR_{\tau}$ where $n$ is the factor by which the exposure time is increased.\\
While the R filter will not be required for this work it is included here as a way to indirectly calculate the required values for the H$_{\alpha}$ filter where the flux will be $1\%$ of the flux in the R filter and hence the SNR $1/10$ of the R filter SNR. The exposure time for the H$_{\alpha}$ filter will be twice that of the broadband filters to compensate for this reduction.\\
Observations will need to be made from approximately $4$am to sunrise barring any timetable restrictions. Towards the night of the $18^{th}$ the moon will be setting closer to dawn as opposed to earlier in the week where it is setting around $3$am, however the target will be higher in the sky on the $18^{th}$. Despite the late setting time however, the moon will still be $\sim$\SI{112}{\degree} away from the target and so should not interfere with observations in any meaningful way.\\
\section*{\large Previous Work}\label{sec:prev}
Prior studies in this area have generated some interesting results which were the focus of a previously submitted literature review.\\
O'Dell et al. $2009$\cite{odell2009} examined data taken from a Hubble Space Telescope observation made in $2009$ as well as similar data taken $\sim9$ years previously. They argued for an homologous expansion of the nebula. They also reported noisy patterns in the motion of individual features pointing to this as further verification of the homologous expansion theory. The observations were made using triple exposures so as to allow for the removal of cosmic-ray events. The images were combined with the IRAF software package. For this study the tangential velocities were derived by first aligning the images, then normalising them so as to ensure that the total signal from both was the same, then taking the ratio of the two. The value they present for the calculated average tangential velocity is $V_{tan}=0.23\pm0.10\times \phi~mas~yr^{-1}$, with $\phi$, the angular distance from the respective objects to the CS and a spatial expansion rate of $V_s=0.65\times \phi_s~kms^{-1}$.\\
O'Dell et al. $2007$\cite{odell2007} determined the gas kinematics, diagnostic and ionic radial profiles, spatial structure and evolutionary phase of the Ring Nebula. Their examination of the expansion velocity of the nebula showed it to be ballistic in nature with $V_{exp}=0.65~km~s^{-1}~arcsec^{-1}$. Their observations were made using long slit spectra directed through an echelle spectrograph. Double exposures were made of each image so as to allow the removal of any cosmic ray events which may have occurred. A variety of images were taken at varying PA and were processed using the IRAF software package. An examination of the line profiles revealed them to be highly non-Gaussian in nature. This, they said, was a clear indication of the complex structure of the nebula most likely caused by the blending of different velocity components.\\
Hartigan et al. $2001$\cite{hartigan2001} use Hubble Space Telescope images in H$_{\alpha}$ and S\RN{2} to reveal the proper motions and changes in the morphology of the HH $111$ jet. This was done by comparing their measurements to results obtained $4$ years previously. They showed that knots in the jet were dominated by emission from nested bow shocks, move ballistically \& that there was no evidence for turbulent motions within the jet. They also showed that shocks can sometimes overtake one another in the jet. Numerical simulations showed that these shocks may arise as the jet interacts with the surrounding medium, previously shocked gas and when fast material overtakes slower material in an unsteady flow. They also observed sharp H$_{\alpha}$ features where the neutral gas becomes collisionally excited upon entering the shock.\\

This work will require the use of several image processing techniques. Some of these, such as image reduction, stacking, generating false colour images have been previously used in laboratory work undertaken by the PI while any new techniques necessary for the successful processing of the images have been outlined in the prior data analysis report and will be briefly covered here.\\
A python script will be used for image reduction. Several functions will be used to load, sort and trim the images as well as performing any required unit conversion. These will produce a set of master images. The reduced object images are generated from these master images. The last step to be performed is the generation of the final stacked image using another function which takes the mean of the reduced set.\\
The methods employed by Peris et al. $2009$\cite{peris2009} will also be relied upon in this work, namely; the Richardson-Lucy (RL) deconvolution algorithm\cite{richardson1972} and the wavelet decomposition technique\cite{mertens2014}. Deconvolution is an algorithmic process for reversing the effects of convolution on recorded data. The algorithm is discussed in the data analysis report and will be applied to the obtained images in order to remove unwanted noise and background interference. Once the image has been properly reduced and deconvolved feature recognition can be achieved by means of the wavelet transforms which will provide a robust set of statistically significant structural patterns\cite{mertens2014} present in the original image. This will be implemented using the python module \lstinline[language=Python]$PyWavelets$\cite{pywavelets}.\\
%\newpage
\vfill
\vfill

{\color{codegray}\hline}
\vspace{5pt}

\footnotesize{
\bibliographystyle{acm}
\bibliography{proposal.bib}
}

\end{document}
